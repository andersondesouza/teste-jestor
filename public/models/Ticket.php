<?php
namespace App\models;

use App\core\Model;

class Ticket extends Model
{
    private $table = 'tickets';

    /**
	 * Find Id
	 *
	 * @param int $id
	 *
	 * @return bool
	 */
	public function findId($id = null)
	{
		if (!is_null($id)) {
			$stmt = $this->db->query("SELECT * FROM {$this->table} WHERE id = {$id}");
			$item = $stmt->fetch(\PDO::FETCH_OBJ);

			if ($item) return $item;
		}
	}

    /**
	 * Find All
	 *
	 * @return array
	 */
	public function findAll()
	{
		try {
			$stmt = $this->db->query("SELECT * FROM {$this->table} ORDER BY id DESC");

			return $stmt->fetchAll(\PDO::FETCH_OBJ);
		} catch (\PDOException $e) {
			die(print_r($e->getMessage()));
		}
    }
    
    /**
	 * Add
	 *
	 * @param array $data
     * 	 
	 * @return bool
	 */
	public function add($data = [])
	{
        if ($data) {
            $title 	        = $data['title'];
            $description    = $data['description'];
            $status 	    = $data['status'];
            $created        = date('Y-m-d H:i:s');
            $modified       = date('Y-m-d H:i:s');

            try {
                $stmt = $this->db->prepare("INSERT INTO {$this->table} (title, description, status, created, modified) 
                    VALUES (:title, :description, :status, :created, :modified)");

                $stmt->bindParam(':title', $title);
                $stmt->bindParam(':description', $description);
                $stmt->bindParam(':status', $status);
                $stmt->bindParam(':created', $created);
                $stmt->bindParam(':modified', $modified);

                $stmt->execute();

                return true;
            } catch (\PDOException $e) {
                return false;
            }	
        }

        return false;
    }

    /**
	 * Edit
	 *
	 * @param array $data
     * @param int $id
     * 	 
	 * @return bool
	 */
	public function edit($data = [], $id)
	{
        if ($data && $id) {            
            $title 	        = $data['title'];
            $description    = $data['description'];
            $status 	    = $data['status'];            
            $modified       = date('Y-m-d H:i:s');

            try {
                $stmt = $this->db->prepare("UPDATE {$this->table} SET title = :title, description = :description, status = :status, modified = :modified WHERE id = {$id}");

                $stmt->bindParam(':title', $title);
                $stmt->bindParam(':description', $description);
                $stmt->bindParam(':status', $status);                
                $stmt->bindParam(':modified', $modified);

                $stmt->execute();

                return true;
            } catch (\PDOException $e) {
                return false;
            }	
        }

        return false;
    }

    /**
	 * Delete
	 *
	 * @param int $id
	 *
	 * @return bool
	 */
	public function delete($id = null)
	{
		if (!is_null($id)) {
			try {
				$stmt = $this->db->prepare("DELETE FROM {$this->table} WHERE id = {$id}");
				$stmt->execute();

				return true;
			} catch (\PDOException $e) {
				return false;
			}			
		}
	}
}