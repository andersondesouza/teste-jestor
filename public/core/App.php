<?php
namespace App\core;

class App
{
    protected $controller   = 'Home';
    protected $method       = 'index';
    protected $page404      = false;
    protected $params       = [];

    public function __construct()
    {
        $parseUrl = $this->parseUrl();

        $this->getController($parseUrl);
        $this->getMethod($parseUrl);
        $this->getParams($parseUrl);

        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    /**
     * Get params this url
     * 
     * @return array
     */
    private function parseUrl()
    {
        return explode('/', substr(filter_input(INPUT_SERVER, 'REQUEST_URI'), 1));
    }

    /**
     * Load the controller if it exists inside the controllers folder
     * 
     * @param array $url
     * 
     * @return void
     */
    private function getController($url = [])
    {
        if (isset($url[0]) && !empty($url[0])) {
            if (file_exists(__DIR__ . '/../controllers/' . ucfirst($url[0]) . '.php')) {
                $this->controller = ucfirst($url[0]);                
            } else {
                $this->page404 = true;
            }
        }

        require __DIR__ . "/../controllers/{$this->controller}.php";

        $class = "\App\controllers\\{$this->controller}";
        
        $this->controller = new $class;
    }
    
    /**
     * Load method
     * 
     * @param array $url
     * 
     * @return void
     */
    private function getMethod($url = [])
    {
        if (isset($url[1]) && !empty($url[1])) {
            if (method_exists($this->controller, $url[1]) && !$this->page404) {
                $this->method = $url[1];
            } else {
                $this->method = 'notFound';
            }
        }
    }

    /**
     * Load params
     * 
     * @param array $url
     * 
     * @return void
     */
    private function getParams($url = [])
    {
        if (count($url) > 2) {
            $this->params = array_slice($url, 2);
        }
    }
}