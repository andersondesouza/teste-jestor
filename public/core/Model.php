<?php 
namespace App\core;

use App\core\Database;

abstract class Model
{
    protected $db;

	public function __construct()
	{
		$conn = new Database;
		$this->db = $conn->conn;
	}
}