<?php
namespace App\controllers;

use App\core\Controller;

class Auth extends Controller
{
    private $user;

    public function __construct()
    {
        $this->user = $this->model('User');
    }

    /**
     * Login
     * 
     * @return void
     */
    public function login()
    {
        $message = '';

        if ($this->isPost()) {
            if ($_POST['username'] && $_POST['password']) {
                $login = $this->user->login($_POST);

                if ($login) {
                    $_SESSION['user'] = $login;

                    header("Location: /tickets");
                    exit(0);
                } else {
                    $_SESSION['user'] = null;
                    $message = '<div class="alert alert-danger" role="alert">Usuário inexistente!</div>';
                }
            }
        }

        $this->view('auth/login', [
            'headTitle' => 'Login',
            'message'   => $message            
        ]);
    }

    /**
     * Logout
     * 
     * @return void
     */
    public function logout()
    {
        session_destroy();

        header("Location: /auth/login");
        exit(0);
    }
}