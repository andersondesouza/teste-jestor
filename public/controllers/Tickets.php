<?php
namespace App\controllers;

use App\core\Controller;

class Tickets extends Controller
{
    private $ticket;

    public function __construct()
    {
        $this->verifyLogin();
        
        $this->ticket = $this->model('Ticket');
    }

    /**
	 * List
	 *
	 * @return void
	 */
    public function index()
    {
        $tickets = $this->ticket->findAll();

        $this->view('tickets/index', [
            'headTitle' => 'Tickets - List',
            'tickets'   => $tickets
        ]);
    }

    /**
	 * Add
	 *
	 * @return void
	 */
    public function add()
    {
        $message = '';

        if ($this->isPost()) {
            $message = ($this->ticket->add($_POST)) 
                ? '<div class="alert alert-primary" role="alert">Registro realizado com sucesso!</div>'
                : '<div class="alert alert-danger" role="alert">Ocorreu um erro ao realizar o registro!</div>';
        }

        $this->view('tickets/add', [
            'headTitle' => 'Tickets - Adicionar',
            'message'   => $message           
        ]);
    }

    /**
	 * Edit
	 *
	 * @return void
	 */
    public function edit()
    {
        $id = $this->getParamsId();

        if (!$id) {
            header("Location: /tickets");
            exit(0);
        }

        $message = '';

        if ($this->isPut()) {
            $message = ($this->ticket->edit($_POST, $id)) 
                ? '<div class="alert alert-primary" role="alert">Registro alterado com sucesso!</div>'
                : '<div class="alert alert-danger" role="alert">Ocorreu um erro ao alterar o registro!</div>';
        }    

        $ticket = $this->ticket->findId($id);

        $this->view('tickets/edit', [
            'headTitle' => 'Tickets - Editar',
            'message'   => $message,
            'ticket'    => $ticket           
        ]);        
    }

    /**
	 * Delete
	 *
	 * @return void
	 */
	public function delete()
	{
        $id = $this->getParamsId();

        if (!$id) {
            header("Location: /tickets");
            exit(0);
        }

        if ($this->isDelete()) {
            $this->ticket->delete($id);
        }

		header("Location: /tickets");
        exit(0);
	}
}