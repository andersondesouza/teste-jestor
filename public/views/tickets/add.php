<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <h1 class="title">Adicionar Ticket</h1>
            <a href="/tickets" class="btn btn-secondary float-right">Listar</a>
        </div>
    </div>

    <?php if ($data['message']) : ?>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <?= $data['message'] ?>
            </div>
        </div>
    <?php endif ?>

    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <form 
                method="POST" 
                action="/tickets/add" 
                data-toggle="validator" 
                role="form">
                <div class="form-row">
                    <div class="form-group col-md-8">
                        <label for="title">Título</label>
                        <input name="title" type="text" class="form-control" id="title" required>
                        <div class="help-block with-errors"></div>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="status">Status</label>
                        <select name="status" id="status" class="form-control" required>
                            <option value="" selected>Escolha</option>
                            <option value="1">Ativo</option>
                            <option value="0">Inativo</option>
                        </select>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="description">Descrição</label>
                        <textarea 
                            class="form-control" 
                            name="description"
                            id="description" 
                            required></textarea>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>

                <div class="form-group row">
                    <div class="form-group col-md-12">
                        <button type="submit" class="btn btn-primary">Cadastrar</button>
                    </div>
                </div>
            </form>            
        </div>
    </div>
</div>