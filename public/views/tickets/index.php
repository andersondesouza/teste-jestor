<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12">
            <h1 class="title">Listar Tickets</h1>
            <a href="/auth/logout" class="btn btn-secondary float-right">Sair</a>
            <a href="/tickets/add" class="btn btn-secondary float-right">Cadastrar</a>
        </div>
    </div>

    <?php if ($data['tickets']) : ?>
        <div class="row">
            <div class="col-xs-12 col-sm-12">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Título</th>
                            <th scope="col">Descrição</th>
                            <th scope="col">Status</th>
                            <th scope="col">Data Criação</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data['tickets'] as $ticket) : ?>
                            <tr>
                                <th scope="row"><?= $ticket->id ?></th>
                                <td><?= $ticket->title ?></td>
                                <td><?= substr($ticket->description, 0, 50) ?>...</td>
                                <td><?= $ticket->status == 0 ? 'Inativo' : 'Ativo' ?></td>
                                <td><?= date('d/m/Y', strtotime($ticket->created)) ?></td>
                                <td class="actions">
                                    <a class="btn btn-secondary" href="/tickets/edit/<?= $ticket->id ?>">Editar</a>
                                    <form action="/tickets/delete/<?= $ticket->id ?>" method="POST">
                                        <input type="hidden" name="_METHOD" value="DELETE"/>
                                        <input class="btn btn-danger" type="submit" value="Excluir">
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    <?php else : ?>
        <h2>Nenhum ticket encontrado!</h2>
    <?php endif ?>
</div>