<div class="container">
    <?php if ($data['message']) : ?>
        <div class="row justify-content-center">        
            <div class="col-xs-12 col-sm-6">
                <?= $data['message'] ?>
            </div>            
        </div>
    <?php endif ?>

    <div class="row justify-content-center">
        
        <div class="col-xs-12 col-sm-6 login">                        
            <img src="/webroot/images/logo.png" />
            
            <form 
                method="POST" 
                action="/auth/login" 
                data-toggle="validator" 
                role="form">
                
                <div class="form-group">
                    <label for="username">Usuário</label>
                    <input type="text" class="form-control" name="username" id="username" required>                
                </div>
                <div class="form-group">
                    <label for="password">Senha</label>
                    <input type="password" class="form-control" name="password" id="password" required>
                </div>
                <button type="submit" class="btn btn-primary">Entrar</button>
            </form>
        </div>
    </div>
</div>