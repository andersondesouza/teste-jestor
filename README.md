# Teste Jestor

##Instalação

Baixe as imagens do Docker
```bash
docker-compose up -d
```
Baixe as dependências do Composer

Primeiro entre na imagem do php-fpm
```bash
docker exec -it teste-jestor-php-fpm bash
```

Depois rode o comando para instalar as dependências do composer
```bash
composer install
```

##Banco de dados

O banco está na raiz do projeto

##Como usar

Em seu navegador, digite o seguinte caminho
```bash
http://localhost:8080/
```

##Conexão

Os dados de conexão está no arquivo
```bash
docker-compose.ylm
```

##Usuário de acesso
```bash
usuário: admin
senha: testejestor
```